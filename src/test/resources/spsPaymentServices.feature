Feature: As a QA Engineer I want to Add, Edit Credit Cards for Subscription and Capture and Sign in Payments.

  Scenario: 
    As a SPS User I want to capture payment from the payment Services - "Capture Paments".

    Given I am Utilizing SPS Payment Service to Get "Capture Payment"
    When I am Calling The Capture Payment Service End Point
    Then I Should Suppose to Get "Success Capture Payment" in response.

  Scenario: 
    As a SPS User I want myself sign in payment from the sign in Payment Services - "Sign in Payment".

    Given I am Utilizing SPS Payment Service to Get "Sign in Payment"
    When I am Calling The Sign in Payment Service End Point
    Then I Should Suppose to Get "Sign in Payment Info" in response.

  Scenario: 
    As a SPS User I want to add credit card from the subscription Services - "Add Credit Card".

    Given I am Utilizing SPS Service to "Add Credit Card".
    When I am Calling Add Credit Card to Subscription Service End Point
    Then I Should Suppose to Get "Success - Credit Card Added" in response.
