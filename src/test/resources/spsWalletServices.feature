Feature: As QA Engineer I want to Add Credit Card to Wallet, Update or Edit Credit Card Information, Get number of Credit Cards in the Wallet
 & it's Info and Delete Credit Card from the Wallet, in order to Validate the End to End Testing of Submitting orders and Make Payments.

Scenario:
As a SPS User I want to add credit card to wallet using sps wallet services - "Create user, Access to Account & Add Credit Card to Wallet". 
	Given I am Using SPS Wallet Service to "Add Crdit Card" 
	When I am Calling the Add Credit Card to Wallet Service End Point 
	Then Should Get the "Added Credit Card In the Wallet" in response.
	
Scenario:
As a SPS User I want to get the number of cards in wallet and its Info using sps wallet services - "Create user, Access to Account, Add Credit Card & Get the Card Info". 
	Given I am Using SPS Wallet Service to "Get Crdit Card Info" 
	When I am Calling the Get Credit Card Service End Point 
	Then Should Get the "Credit Card Info from the Wallet" in response. 

Scenario:
As a SPS User I want to delete a credit card from the wallet using sps wallet services - "Create user, Access to Account, Add Credit Card & Delete the Card". 
	Given I am Using SPS Wallet Service to "Delete Crdit Card" 
	When I am Calling the Delete Credit Card Service End Point 
	Then Should Get the "Deleted - No Credit Card Info" in response. 