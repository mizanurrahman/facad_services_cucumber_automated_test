Feature: As QA Engineer I want to change user's existing school to existing International School, Home school, Manual School or 
send a request to customer service to review my school Info because of the school name is correct, but the address is incorrect.
         In order to Validate the End to End Testing of edit user's school information.

Scenario:
As a SPS User I want to change existing US domestic school to existing international school using sps edit school services - "Create Teacher attached with US Domestic School, Access to Account & Change the School by Add existing International School Id".
 
	Given I am Using SPS Edit School Service to Change from US Domestic School to "Existing International School"
	When I am Calling the Edit Domestic To International School Service End Point
	Then I should See the School changed to "International School" in response.
	
Scenario:
As a SPS User I want to change existing US domestic school to home School using sps edit school services - "Create Teacher attached with US Domestic School, Create Home School, Access to Account & Change the School by Adding the Home School".

	Given I am Using SPS Edit School Service to Change from US Domestic School to "Home School"
	When I am Calling the Edit Domestic To Home School Service End Point
	Then I should See the School changed to "Home School" in response.
	
Scenario:
As a SPS User I want to change existing US domestic school to manual School using sps edit school services - "Create Teacher attached with US Domestic School, Create Manual School, Access to Account & Change the School by Adding the Manual School".
 
	Given I am Using SPS Edit School Service to Change from US Domestic School to "Manual School" 
	When I am Calling the Edit Domestic To Manual School Service End Point 
	Then I should See the School changed to "Manual School" in response.
	
Scenario:
As a SPS User I want to send my school info using sps edit school services to send a request to customer service to review my school Info - "Create Teacher attached with US Domestic School, Access to Account & Send School Info for Review".

	Given I am Using SPS Edit School Service to Send request for "Review" 
	When I am Calling the Edit School Service for Review End Point 
	Then I should Get the Request for "Review Sent" in response.
	