package com.scholastic.test;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;


import com.scholastic.main.SpsWalletServiceSupportClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepdefinationSpsWalletServicesTest extends SpsWalletServiceSupportClass
{	
	public SpsWalletServiceSupportClass supportClasss=new SpsWalletServiceSupportClass();
	
	@Given("^I am Using SPS Wallet Service to (.*)$")
	public void i_am_Using_SPS_Wallet_Service_to(String x) throws Throwable 
	{
		System.out.println(x);
	}
	// ########## Add Credit Card to Wallet ##########
	@When("^I am Calling the Add Credit Card to Wallet Service End Point$")
	public void i_am_Calling_the_Add_Credit_Card_to_Wallet_Service_End_Point() throws Throwable 
	{
		//Create Teacher
		ExtractableResponse<Response> createTeacherResponse=
				given()
						//.log().all()
						.contentType("application/json")
						.body(createTeacherPayload()).
				when()
						.post(ENDPOINT_TEACHER_REGISTRATION).
				then()
						.statusCode(201)
						.spec(createTeacherResponseValidator())
						.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");
		user=createTeacherResponse.path("userName");
		//System.out.println(">>>>>>>Teacher SPSID is: "+teacherSPSID);
		//System.out.println(">>>>>>>User Name is: "+user);
		
		Thread.sleep(6000L);
		
		//Authentication
		ExtractableResponse<Response> createLogInResponse=
				given()
						.contentType("application/json")
		                .body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		//System.out.println(createLogInResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
		
		//System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		//System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		//System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
							
		// Add Credit card to wallet.
		
		//ExtractableResponse<Response> addCreditCardToWalletResponse=
				given()
						//.log().all()
						.pathParam("spsId",userSPSID)
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp)
						.contentType("application/json")
						.body(addCreditCardToWalletPayload()).
				when()
						.post(ENDPOINT_ADD_CREDITCARD).
				then()
						.statusCode(200)
						//.spec(addCreditCardToWalletResponseValidator())
						.extract();
		//System.out.println("******************** AddCredit Card To Wallet Response *********************");
		//System.out.println(addCreditCardToWalletResponse.asString());
		//System.out.println("****************************************************************************");
			
	}
	// ########## Get Credit Card Info ##########
	
	@When("^I am Calling the Get Credit Card Service End Point$")
	public void i_am_Calling_the_Get_Credit_Card_Service_End_Point() throws Throwable 
	{
		//Create Teacher
		ExtractableResponse<Response> createTeacherResponse=
				given()
						//.log().all()
						.contentType("application/json")
						.body(createTeacherPayload()).
				when()
						.post(ENDPOINT_TEACHER_REGISTRATION).
				then()
						.statusCode(201)
						.spec(createTeacherResponseValidator())
						.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");
		user=createTeacherResponse.path("userName");
		//System.out.println(">>>>>>>Teacher SPSID is: "+teacherSPSID);
		//System.out.println(">>>>>>>User Name is: "+user);
		
		Thread.sleep(6000L);
		//Authentication
		ExtractableResponse<Response> createLogInResponse=
				given()
						.contentType("application/json")
		                .body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		//System.out.println(createLogInResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
		
		//System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		//System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		//System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
		
		// Add Credit card to wallet.
		
		//ExtractableResponse<Response> addCreditCardToWalletResponse=
				given()
						//.log().all()
						.pathParam("spsId",userSPSID)
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp)
						.contentType("application/json")
						.body(addCreditCardToWalletPayload()).
				when()
						.post(ENDPOINT_ADD_CREDITCARD).
				then()
						.statusCode(200)
						//.spec(addCreditCardToWalletResponseValidator())
						.extract();
		
		//System.out.println(addCreditCardToWalletResponse.asString());
					
		// Get the Credit Card
		
		ExtractableResponse<Response> getCreditCardResponse=
				given()
						.pathParam("spsId",teacherSPSID).
				when()
						.get(ENDPOINT_GET_CREDITCARD).
				then()
						.statusCode(200)
						.spec(getCreditCardResponseValidator())
						.extract();
		totalcreditcardCount= getCreditCardResponse.path("count");
		int walletId=Integer.parseInt((String) getCreditCardResponse.path("wallet.id[0]"));
		walletID.add(walletId);
		//System.out.println(">>>>>>>>> Wallet Id is: "+walletId);
		//System.out.println("@#######"+walletID.get(0));
		//System.out.println(">>>>>>>>> Total Credit Card Count is: "+totalcreditcardCount);
		//System.out.println("******************** Get Credit Card Info From Wallet *********************");
		//System.out.println(getCreditCardResponse.asString());
		//System.out.println("****************************************************************************");
	}
	
	// ########## Delete Credit Card from Wallet ##########
	@When("^I am Calling the Delete Credit Card Service End Point$")
	public void i_am_Calling_the_Delete_Credit_Card_Service_End_Point() throws Throwable 
	{
		//Create Teacher
		ExtractableResponse<Response> createTeacherResponse=
				given()
						//.log().all()
						.contentType("application/json")
						.body(createTeacherPayload()).
				when()
						.post(ENDPOINT_TEACHER_REGISTRATION).
				then()
						.statusCode(201)
						.spec(createTeacherResponseValidator())
						.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");
		user=createTeacherResponse.path("userName");
		//System.out.println(">>>>>>>Teacher SPSID is: "+teacherSPSID);
		//System.out.println(">>>>>>>User Name is: "+user);
			
		Thread.sleep(6000L);
			
		//Authentication
		ExtractableResponse<Response> createLogInResponse=
				given()
						.contentType("application/json")
			            .body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		//System.out.println(createLogInResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
			
		//System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		//System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		//System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
			
		// Add Credit card to wallet.
			
		//ExtractableResponse<Response> addCreditCardToWalletResponse=
				given()
						//.log().all()
						.pathParam("spsId",userSPSID)
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp)
						.contentType("application/json")
						.body(addCreditCardToWalletPayload()).
				when()
						.post(ENDPOINT_ADD_CREDITCARD).
				then()
						.statusCode(200)
						//.spec(addCreditCardToWalletResponseValidator())
						.extract();
			
		//System.out.println("******************** Response After Adding Credit Card To Wallet *********************");
		//System.out.println(addCreditCardToWalletResponse.asString());
		//System.out.println("**************************************************************************************");			
		// Delete Credit Card From Wallet
			
		//ExtractableResponse<Response> deleteCreditCardResponse=
				given()
						.pathParam("spsId",teacherSPSID).
				when()
						.delete(ENDPOINT_DELETE_CREDITCARD).
				then()
						.statusCode(200)
						//.spec(deleteCreditCardResponseValidator())
						.extract();
		//System.out.println("******************** Response After Deleting Credit Card From Wallet *********************");
		//System.out.println(deleteCreditCardResponse.asString());
		//System.out.println("*******************************************************************************************");
	}
	@Then("^Should Get the (.*) in response.$")
	public void Should_Get_the_Added_Credit_Info_in_response(String x) throws Throwable 
	{
		System.out.println(x);
	}
	
}
