package com.scholastic.test;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.scholastic.main.SpsPaymentServiceSupportClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepdefinationSpsPaymentServicesTest extends SpsPaymentServiceSupportClass
{
	public SpsPaymentServiceSupportClass supportClass=new SpsPaymentServiceSupportClass();
	
	@Given("^I am Utilizing SPS Payment Service to Get(.*)$")
	public void i_am_Utilizing_SPS_Payment_Service_to_Get(String x) throws Throwable 
	{
	   System.out.println(x);
	}

	@When("^I am Calling The Capture Payment Service End Point$")
	public void i_am_Using_The_Capture_Payment_Service_End_Point() throws Throwable 
	{
		//Authentication
				ExtractableResponse<Response> createLogInResponse=
						given()
								.contentType("application/json")
				                .body(createLogInPayload()).
						when()
								.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
						then()
								.statusCode(200)
								.extract();	
				sps_session=createLogInResponse.path("SPS_SESSION.value");
				sps_tsp=createLogInResponse.path("SPS_TSP.value");
				//System.out.println(createLogInResponse.path("SPS_UD.value"));
				userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
				
				//System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
				//System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
				//System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
							
				// Capture Payment
				//ExtractableResponse<Response> createCapturePaymentResponse=
						given()
								//.log().all()
								.pathParam("spsId",userSPSID)
								.cookies("SPS_SESSION",sps_session)
								.cookie("SPS_TSP",sps_tsp)
								.contentType("application/json")
								.body(createCapturePaymentPayload()).
						when()
								.post(ENDPOINT_CAPTURE_PAYMENT).
						then()
								.statusCode(200)
								//.spec(createCapturePaymentResponse())
								.extract();
				//userSPSID=createCapturePaymentResponse.path("spsId");	
				//System.out.println("User SPSID is: "+userSPSID);
				//System.out.println("******************** Captured Payment Response ********************");
				//System.out.println(createCapturePaymentResponse.asString());
				//System.out.println("*******************************************************************");
	}

	@When("^I am Calling The Sign in Payment Service End Point$")
	public void i_am_Calling_The_Sign_in_Payment_Service_End_Point() throws Throwable {
		//Authentication
				ExtractableResponse<Response> createLogInResponse=
						given()
								.contentType("application/json")
				                .body(createLogInPayload()).
						when()
								.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
						then()
								.statusCode(200)
								.extract();	
				sps_session=createLogInResponse.path("SPS_SESSION.value");
				sps_tsp=createLogInResponse.path("SPS_TSP.value");
				//System.out.println(createLogInResponse.path("SPS_UD.value"));
				userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
				
				//System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
				//System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
				//System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
							
				// Sign a Payment
				//ExtractableResponse<Response> createSignPaymentResponse=
						given()
								//.log().all()
								.pathParam("spsId",userSPSID)
								.cookies("SPS_SESSION",sps_session)
								.cookie("SPS_TSP",sps_tsp)
								.contentType("application/json")
								.body(createSignPaymentPayload()).
						when()
								.post(ENDPOINT_SIGN_PAYMENT).
						then()
								.statusCode(200)
								//.spec(createCapturePaymentResponse())
								.extract();
				//userSPSID=createCapturePaymentResponse.path("spsId");	
				//System.out.println("User SPSID is: "+userSPSID);
				//System.out.println("******************** Signed Payment Response ********************");
				//System.out.println(createSignPaymentResponse.asString());
				//System.out.println("*******************************************************************");
	}
	
		

	@Given("^I am Utilizing SPS Service to (.*).$")
	public void i_am_Utilizing_SPS_Service_to (String y) throws Throwable 
	{
		System.out.println(y);   
	}

	@When("^I am Calling Add Credit Card to Subscription Service End Point$")
	public void i_am_Calling_Add_Credit_Card_to_Subscription_Service_End_Point() throws Throwable 
	{
		//Authentication
				ExtractableResponse<Response> createLogInSubsResponse=
						given()
								.contentType("application/json")
				                .body(createLogInSubsPayload()).
						when()
								.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
						then()
								.statusCode(200)
								.extract();	
				sps_session=createLogInSubsResponse.path("SPS_SESSION.value");
				sps_tsp=createLogInSubsResponse.path("SPS_TSP.value");
				//System.out.println(createLogInSubsResponse.path("SPS_UD.value"));
				userSPSID=((String)createLogInSubsResponse.path("SPS_UD.value")).split("\\|")[0];
				
				//System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
				//System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
				//System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
							
				// Add Credit Card To Subscription
				ExtractableResponse<Response> addCreditCardToSubscriptionResponse=
						given()
								//.log().all()
								.pathParam("spsId",userSPSID)
								.contentType("application/json")
								.cookies("SPS_SESSION",sps_session)
								.cookie("SPS_TSP",sps_tsp)
								.body(addCreditCardToSubscriptionPayload()).
						when()
								.post(ENDPOINT_ADDCREDITCARD_SUBSCRIPTION).
						then()
								.statusCode(202)
								//.spec(createCapturePaymentResponse())
								.extract();
				//userSPSID=createCapturePaymentResponse.path("spsId");	
				//System.out.println("User SPSID is: "+userSPSID);
				//System.out.println("******************** Add Credit Card To Subscription Response ********************");
				//System.out.println(addCreditCardToSubscriptionResponse.asString());
				//System.out.println("**********************************************************************************");
	}

	@Then("^I Should Suppose to Get (.*) in response.$")
	public void i_Should_Suppose_to_Get_in_response(String x) throws Throwable
	{
		System.out.println(x);
	}
	
}
